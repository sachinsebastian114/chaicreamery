# Project Name

ChaiCreamery Web Application

## Folder Structure

- `cafe/`: Contains the main application files.
  - `static/`: Static files for the application (e.g., CSS, images).
  - `templates/`: HTML templates for the application.
  - `Dockerfile`: Dockerfile for building the application.
  - `procfile`: Procfile for defining the application's processes.
  - `main.py`: Main Python file for the application.
  - `requirements.txt`: List of Python dependencies.
  

-  tests/: Contains testing files
- `database/`: Contains files related to the database.
  - `Dockerfile`: Dockerfile for building the database container.
  - `init.sql`: SQL script for initializing the database.

- `workflows/`: Contains GitLab CI/CD configuration files.

## Installation

Clone the Repository:
git clone https://gitlab.com/sachinsebastian114/chaicreamery.git
cd chai creamery

Install Dependencies:
pip install Flask~=2.2.2 WTForms~=3.0.1 Flask-MySQLdb==0.2.0 Flask-Bootstrap==3.3.7.1 Flask-WTF>=0.14.3

Set Up MySQL:
Install MySQL server in your local machine.
Create a MySQL database named cafe.
Set the MySQL username and password in the Flask app (app.config['MYSQL_USER'] = 'your_username', app.config['MYSQL_PASSWORD'] = 'your_password')

Run Chai Creamery:
python main.py

Access the Application:
Open a web browser and go to http://localhost:5000 to access the application.


## Usage

Getting Into the App:
Make sure the application is active. If not, run python app.py to launch the program.
To view the program, launch a web browser and go to http://localhost:5000.
Observing Cafe's in your area
You may see a list of cafés with their specifics, like name, address, and price rating, on the main page.
Integrating a Cafe:

Click the "Add Cafe" item in the navigation menu to add a new café.
After providing the necessary information (name, location, etc.), click "Submit" to complete the form.
The list of cafés on the main page will include the new café with all the informations provided in the Add form.


## Build 

To construct a Docker image for the database component, utilize the provided Dockerfile and tag it with the most recent version.

Drive Database Image: Transfer the just constructed database image to the Google Cloud Artifact Registry.

To construct a Docker image for the café component, utilize the provided Dockerfile and tag it with the most recent version.

Created Cafe Image: Upload the recently created cafe image to the Google Cloud Artifact Registry directly.

Consequently, the most recent database and café Docker image versions may be found in the Google Cloud Artifact Registry, ready for deployment.


## Deployment

Pulls the latest version of the database and cafe application Docker image from the Artifact Registry.

Removes any existing container named database-container or app-container. This step ensures that there are no conflicts when creating a new container for the database.

Removes the local copy of the database and cafe Docker image. This step ensures that the latest version of the image is used for deployment.

Runs a new container for the database using the latest database Docker image. This command maps port 5000 on the host to port 5000 in the container.

Runs a new container for the cafe application using the latest cafe Docker image. This command maps port 3306 on the host to port 3306 in the container.

Both containers are running on same customized network 'project'.


## Testing

Chai Creamery includes automated testing stages to ensure the quality and security of the application:

- **SAST (Static Application Security Testing) **: Static analysis is performed on the codebase to identify potential security   vulnerabilities.
- **White-box Tests**: Unit tests that verify the behavior of individual components or functions.
- **Black-box Tests**: Tests that simulate user interactions to validate the application's functionality.
- **Integration Tests**: Tests that verify the interaction between different application components.


## Contributing

We welcome contributions to the Chai Creamery application! Whether you're fixing a bug, implementing a new feature, or improving documentation, we appreciate your help.

To contribute to the project, follow these steps:
1. **Fork** the repository to your GitHub account.
2. **Clone** your fork to your local machine:
   ```bash
   git clone https://github.com/your-username/chai-creamery.git
Create a new Branch for the changes
Make the changes and test them thoroughly.
Make changes and test them thoroughly.
Commit changes with a descriptive commit message:
git commit -m "Add new feature"

